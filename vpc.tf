resource "aws_key_pair" "andiabdur_key" {
  key_name   = "terraform-demo"
  public_key = "${file("terraform_pubkey.pub")}"
}

resource "aws_vpc" "aab_vpc" {
    cidr_block = "10.8.0.0/16"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
	tags = {
    "Name" = "aab_VPC"
  }
}

resource "aws_subnet" "subnet-public" {
    vpc_id = "${aws_vpc.aab_vpc.id}"
    cidr_block = "10.8.1.0/24"
    availability_zone = "ap-southeast-1b"
    tags = {
    "Name" = "subnet_public"
  }
}
resource "aws_subnet" "subnet-private" {
    vpc_id = "${aws_vpc.aab_vpc.id}"
    cidr_block = "10.8.2.0/24"
    availability_zone = "ap-southeast-1b"
    tags = {
    "Name" = "subnet_private"
  }
}

resource "aws_internet_gateway" "aab_vpc_igw" {
	vpc_id = "${aws_vpc.aab_vpc.id}"
	tags = {
    "Name" = "AAB_VPC_Internet_Gateway"
  }
}  

resource "aws_eip" "address" {
	tags = {
    "Name" = "Elastic_IP"
  }
}

resource "aws_nat_gateway" "gw" {
	allocation_id = "${aws_eip.address.id}"
	subnet_id = "${aws_subnet.subnet-private.id}"
	tags = {
    "Name" = "NAT Gateway"
  }
}

resource "aws_route_table" "aab_vpc_public" {
	vpc_id = "${aws_vpc.aab_vpc.id}"
	
	route {
		cidr_block = "0.0.0.0/0"
		gateway_id = "${aws_internet_gateway.aab_vpc_igw.id}"
	}
	tags = {
    "Name" = "Subnet_Route_Table_untuk_vpc"
  }
}
resource "aws_route_table_association" "aab_vpc_public" {
	subnet_id = "${aws_subnet.subnet-private.id}"
	route_table_id = "${aws_route_table.aab_vpc_public.id}"
}

resource "aws_security_group" "allow_http" {
	name 		= "allow_http"
	description	= "Allow HTTP Inbound connections"
	vpc_id = "${aws_vpc.aab_vpc.id}"
	
	ingress {
		from_port	= 80
		to_port 	= 80
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	ingress {
		description	= "allow ssh"
		from_port	= 22
		to_port 	= 22
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	egress {
		from_port	= 0
		to_port 	= 0
		protocol	= "-1"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	tags = {
    "Name" = "Allow HTTP dan SSH security group"
	}
}
resource "aws_launch_configuration" "web" {
	name_prefix = "web"
	
	 image_id = "${lookup(var.AMIS, var.AWS_REGION)}"
	 instance_type = "t2.medium"
	 key_name = "${aws_key_pair.andiabdur_key.key_name}"
	 
	security_groups = ["${aws_security_group.allow_http.id}"]
	associate_public_ip_address = true
	
	user_data = "${file("install_apache.sh")}"
	lifecycle {
		create_before_destroy = true
	}
}

resource "aws_security_group" "elb_http" {
	name		= "elb_http"
	description	= "Allow HTTP traffic ke instance dari LB,dan ssh"
	vpc_id		= "${aws_vpc.aab_vpc.id}"
	
	ingress {
		from_port	= 80
		to_port 	= 80
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	ingress {
		description	= "allow ssh"
		from_port	= 22
		to_port 	= 22
		protocol	= "tcp"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	egress {
		from_port	= 0
		to_port 	= 0
		protocol	= "-1"
		cidr_blocks	= ["0.0.0.0/0"]
	}
	tags = {
    "Name" = "Allow_HTTP_dan_SSH_security_group"
	}
}
resource "aws_elb" "web_elb" {
	name = "web-elb"
	security_groups	= ["${aws_security_group.elb_http.id}"]
	subnets 		= ["${aws_subnet.subnet-private.id}"]
	
	cross_zone_load_balancing = true
	health_check {
		healthy_threshold = 2
		unhealthy_threshold = 2
		timeout = 3
		interval = 30
		target = "HTTP:80/"
	}
	listener {
	lb_port = 80
	lb_protocol = "http"
	instance_port = "80"
	instance_protocol = "http"
	}
}

resource "aws_autoscaling_group" "web" {
	name = "${aws_launch_configuration.web.name}-asg"
	
	min_size = 2
	desired_capacity = 2
	max_size = 5
	health_check_type = "ELB"
	load_balancers = ["${aws_elb.web_elb.id}"]
	
	launch_configuration = "${aws_launch_configuration.web.name}"
	
	enabled_metrics = [
		"GroupMinSize",
		"GroupMaxSize",
		"GroupDesiredCapacity",
		"GroupInServiceInstances",
		"GroupTotalInstances"
	]
	
	metrics_granularity = "1Minute"
	
	vpc_zone_identifier = [
		"${aws_subnet.subnet-private.id}"
	]
	
	lifecycle {
		create_before_destroy = true
	}
	
	tag {
		key = "Name"
		value = "web"
		propagate_at_launch = true
	}
}

resource "aws_autoscaling_policy" "web_policy_up" {
	name = "web_policy_up"
	scaling_adjustment = 1
	adjustment_type = "ChangeInCapacity"
	cooldown = 300
	autoscaling_group_name = "${aws_autoscaling_group.web.name}"
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
	alarm_name = "web_cpu_alarm_up"
	comparison_operator = "GreaterThanOrEqualToThreshold"
	evaluation_periods = "2"
	metric_name = "CPUUtilization"
	namespace = "AWS/EC2"
	period = "120"
	statistic = "Average"
	threshold = "45"
	
	dimensions = {
		AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
	}
	
	alarm_description = "This Metric Monitor EC2 Instance CPU Utilization"
	alarm_actions = ["${aws_autoscaling_policy.web_policy_up.arn}"]
}

resource "aws_autoscaling_policy" "web_policy_down" {
	name = "web_policy_down"
	scaling_adjustment = -1
	adjustment_type = "ChangeInCapacity"
	cooldown = 300
	autoscaling_group_name = "${aws_autoscaling_group.web.name}"
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_down" {
	alarm_name = "web_cpu_alarm_down"
	comparison_operator = "LessThanOrEqualToThreshold"
	evaluation_periods = "2"
	metric_name = "CPUUtilization"
	namespace = "AWS/EC2"
	period = "120"
	statistic = "Average"
	threshold = "10"
	
	dimensions = {
		AutoScalingGroupName = "${aws_autoscaling_group.web.name}"
	}
	alarm_description = "This Metric Monitor EC2 Instance CPU Utilization"
	alarm_actions = ["${aws_autoscaling_policy.web_policy_down.arn}"]
}