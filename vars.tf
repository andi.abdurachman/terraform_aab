variable "AWS_REGION" {    
    default = "ap-southeast-1"
}

variable "AMIS" {
	type = map(string)
	default = {
		ap-southeast-1	 = "ami-05b891753d41ff88f"
	}
}